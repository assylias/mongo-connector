/*
 * Copyright 2013 Yann Le Tallec.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.assylias.mongo.connector;

import java.math.BigInteger;
import java.security.SecureRandom;
import java.util.Arrays;
import mockit.Mock;
import mockit.MockUp;
import static org.testng.Assert.*;
import org.testng.annotations.Test;

/**
 *
 */
public class PasswordsTest {

    /**
     * Not testing randomness, which is assumed if several calls yield different results (i.e. the method works)
     */
    @Test
    public void salt() {
        byte[] salt1 = Passwords.getNextSalt();
        assertNotEquals(new BigInteger(salt1), 0);
        byte[] salt2 = Passwords.getNextSalt();
        assertNotEquals(new BigInteger(salt1), new BigInteger(salt2));
    }

    @Test
    public void hash() {
        byte[] salt = Passwords.getNextSalt();
        char[] pwd = "password".toCharArray();
        byte[] hash = Passwords.hash(pwd.clone(), salt);
        assertTrue(Passwords.isExpectedPassword(pwd.clone(), salt, hash));
        assertFalse(Passwords.isExpectedPassword(pwd.clone(), salt, Arrays.copyOf(hash, hash.length - 1)));
        assertFalse(Passwords.isExpectedPassword("password1".toCharArray(), salt, hash));
    }

    @Test
    public void passwordIsNullified() {
        byte[] salt = Passwords.getNextSalt();
        char[] pwd = "password".toCharArray();
        byte[] hash = Passwords.hash(pwd, salt);
        for (char c : pwd) {
            assertEquals(c, 0);
        }
        pwd = "password".toCharArray();
        Passwords.isExpectedPassword(pwd, salt, hash);
        for (char c : pwd) {
            assertEquals(c, 0);
        }
    }

    /**
     * Make sure that all letters and digits are produced, and nothing else
     */
    @Test
    public void generatePasswords() {
        new MockUp<SecureRandom> () {
            private int i = 0;
            @Mock public int nextInt(int n) {
                return i++ % n;
            }
        };
        String s = Passwords.generateRandomPassword(65);
        assertEquals(s, "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ012");
    }

}
