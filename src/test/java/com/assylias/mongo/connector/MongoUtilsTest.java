/*
 * Copyright 2013 Yann Le Tallec.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.assylias.mongo.connector;

import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import java.io.IOException;
import static org.testng.Assert.*;
import org.testng.annotations.Test;

/**
 *
 */
public class MongoUtilsTest {

    @Test
    public void backAndForth() throws IOException {
        DBObject expected = new BasicDBObject();
        DBObject props = new BasicDBObject();
        expected.put("user", "username");
        expected.put("properties", props);

        props.put("first", "first");
        props.put("last", "last");
        props.put("group", "Administrator");

        User user = new User("username", "first", "first", "last", "last", "group", "Administrator");
        DBObject actual = MongoUtils.getDbObject(user);
        actual.removeField("_id");

        assertTrue(actual.keySet().containsAll(expected.keySet()));
        assertTrue(expected.keySet().containsAll(actual.keySet()));
        for (String key : actual.keySet()) {
            assertEquals(actual.get(key), expected.get(key));
        }

        User user2 = MongoUtils.getPojo(actual, User.class);
        assertEquals(user2, user);
    }
}