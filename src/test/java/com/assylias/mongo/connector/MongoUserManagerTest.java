/*
 * Copyright 2013 Yann Le Tallec.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.assylias.mongo.connector;

import com.mongodb.DB;
import com.mongodb.MongoException;
import org.bson.types.ObjectId;
import static org.testng.Assert.*;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

/**
 *
 */
public class MongoUserManagerTest extends MongoManagerTest<User> {

    private DB db;

    @BeforeClass
    public void beforeClass() throws Exception {
        super.beforeClass("users", User.class);
        db = mongo.getDBCollection().getDB();
    }

    @BeforeMethod
    public void beforeMethod() throws Exception {
        mongo.getMongoCollection().remove("{user: \"jdoe\"}");
        mongo.getMongoCollection().remove("{user: \"jdoe2\"}");
        mongo.getMongoCollection().remove("{user: \"jdoeXXX\"}");
        assertTrue(mongo.getDBCollection().count() == 0);
    }

    @Test
    public void createUser() throws Exception {
        MongoUserManager um = new MongoUserManager(db);
        User user = new User("jdoe", "first", "John", "last", "Doe", "group", "Administrator");
        um.create(user, "password".toCharArray());
        User stored = um.get("jdoe");
        assertEquals(stored, user);
    }

    @Test
    public void createDefaultUser() throws Exception {
        MongoUserManager um = new MongoUserManager(db);
        User user = DefaultUser.username("jdoe").first("John").last("Doe").inGroups("Administrator", "User")
                .withPermissions("Edit", "Delete").email("abc@a.com").with("age", 32).create();
        um.create(user, "password".toCharArray());
        User stored = um.get("jdoe");
        assertEquals(stored, user);
    }

    @Test(expectedExceptions = MongoException.DuplicateKey.class)
    public void createDuplicateUser() throws Exception {
        MongoUserManager um = new MongoUserManager(db);
        User user1 = new User("jdoe", "first", "John", "last", "Doe", "group", "Administrator");
        User user2 = new User("jdoe", "first", "John2", "last", "Doe2", "group", "Administrator");
        um.create(user1, "password".toCharArray());
        um.create(user2, "password".toCharArray());
    }

    @Test(expectedExceptions = MongoException.class)
    public void createUserNetworkDown() throws Exception {
        MongoUserManager um = new MongoUserManager(db);
        User user = new User("jdoe", "first", "John", "last", "Doe", "group", "Administrator");
        TestUtils.mongoInsertThrows(mongo.getDBCollection());
        um.create(user, "password".toCharArray());
    }

    @Test
    public void getUnknownUser() throws Exception {
        MongoUserManager um = new MongoUserManager(db);
        assertEquals(um.get("UNKWON_USER"), null);
    }

    @Test
    public void getAllEmpty() throws Exception {
        MongoUserManager um = new MongoUserManager(db);
        assertTrue(um.getAll().isEmpty());
    }

    @Test
    public void amendUser() throws Exception {
        MongoUserManager um = new MongoUserManager(db);
        User oldUser = new User("jdoe", "first", "John", "last", "Doe", "group", "Administrator");
        ObjectId id = um.create(oldUser, "password".toCharArray());
        User newUser = new User("jdoe", "first", "John", "last", "DoeBetter", "group", "Administrator");
        um.amend(id, newUser);

        //new is there
        User stored = um.get("jdoe");
        assertEquals(stored, newUser);
        assertEquals(um.getAll().size(), 1); //old is not there any more
    }

    @Test(expectedExceptions = IllegalArgumentException.class)
    public void amendInvalidUser() throws Exception {
        MongoUserManager um = new MongoUserManager(db);
        User user = new User("INVALID", "first", "John", "last", "Doe", "group", "Administrator");
        um.amend(new ObjectId(), user);
    }

    @Test
    public void changePassword() throws Exception {
        MongoUserManager um = new MongoUserManager(db);
        User user = new User("jdoe", "first", "John", "last", "Doe", "group", "Administrator");
        um.create(user, "password".toCharArray());
        assertTrue(um.authenticate(user, "password".toCharArray()));

        um.changePassword(user, "password2".toCharArray());

        assertFalse(um.authenticate(user, "password".toCharArray()));
        assertTrue(um.authenticate(user, "password2".toCharArray()));
    }

    @Test(expectedExceptions = IllegalArgumentException.class)
    public void changePasswordInvalidUser() throws Exception {
        MongoUserManager um = new MongoUserManager(db);
        User user = new User("jdoe", "first", "John", "last", "Doe", "group", "Administrator");
        um.changePassword(user, "password2".toCharArray());
    }

    @Test
    public void authenticate() throws Exception {
        MongoUserManager um = new MongoUserManager(db);
        User user = new User("jdoe", "first", "John", "last", "Doe", "group", "Administrator");
        um.create(user, "password".toCharArray());
        assertTrue(um.authenticate(user, "password".toCharArray()));
        assertFalse(um.authenticate(user, "pasSword".toCharArray()));
        assertTrue(um.authenticate("jdoe", "password".toCharArray()));
        assertFalse(um.authenticate("jdoe", "pasSword".toCharArray()));
        assertFalse(um.authenticate("jdoeXXX", "pasSword".toCharArray()));
    }

    @Test
    public void authenticateNoPassword() throws Exception {
        MongoUserManager um = new MongoUserManager(db);
        User user = new User("jdoe", "first", "John", "last", "Doe", "group", "Administrator");
        um.create(user, new char[]{});
        assertTrue(um.authenticate(user, new char[]{}));
    }

    @Test
    public void getUsers() throws Exception {
        MongoUserManager um = new MongoUserManager(db);
        assertTrue(um.getAll().isEmpty());
        User user = new User("jdoe", "first", "John", "last", "Doe", "group", "Administrator");
        um.create(user, "password".toCharArray());

        assertTrue(um.getAll().contains(user));

        User user2 = new User("jdoe2", "first", "John2", "last", "Doe2", "group", "Administrator");
        um.create(user2, "password".toCharArray());

        assertTrue(um.getAll().contains(user));
        assertTrue(um.getAll().contains(user2));
    }
}
