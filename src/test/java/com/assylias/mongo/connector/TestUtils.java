/*
 * Copyright 2013 Yann Le Tallec.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.assylias.mongo.connector;

import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBPort;
import java.io.IOException;
import mockit.Mock;
import mockit.MockUp;

/**
 *
 */
public class TestUtils {

    public static void mongoInsertThrows(DBCollection collection) {

        new MockUp<DBPort>() {
            @Mock
            void checkAuth(DB db) throws IOException {
                throw new IOException();
            }
        };
    }
}
