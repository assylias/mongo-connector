/*
 * Copyright 2013 Yann Le Tallec.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.assylias.mongo.connector;

import java.util.Arrays;
import org.testng.annotations.Test;
import static org.testng.Assert.*;

public class DefaultUserTest {

    @Test
    public void createUser() {
        DefaultUser u = DefaultUser.username("jdoe")
                            .first("John")
                            .last("Doe")
                            .inGroups("Administrator", "User")
                            .withPermissions("Edit", "Delete")
                            .with("Age", 32).create();
        assertEquals(u.getUser(), "jdoe");
        assertEquals(u.getFirstName(), "John");
        assertEquals(u.getLastName(), "Doe");
        assertEquals(u.getGroups().size(), 2);
        assertTrue(u.getGroups().containsAll(Arrays.asList("Administrator", "User")));
        assertEquals(u.getPermissions().size(), 2);
        assertTrue(u.getPermissions().containsAll(Arrays.asList("Edit", "Delete")));
        assertEquals(u.get("Age"), 32);
        assertTrue(u.hasPermission("Edit"));
        assertTrue(u.hasPermission("Delete"));
        assertFalse(u.hasPermission("Other"));
        assertTrue(u.isInGroup("Administrator"));
        assertTrue(u.isInGroup("User"));
        assertFalse(u.isInGroup("Other"));
    }

    @Test(expectedExceptions = IllegalArgumentException.class)
    public void invalidKey() {
        DefaultUser u = DefaultUser.username("").with("first", "first").create();
    }

}