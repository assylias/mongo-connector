/*
 * Copyright 2013 Yann Le Tallec.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.assylias.mongo.connector;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

/**
 * A User with a default predefined set of properties.
 */
@SuppressWarnings("unchecked")
public class DefaultUser extends User {

    private static final String FIRST_KEY = "first";
    private static final String LAST_KEY = "last";
    private static final String EMAIL_KEY = "email";
    private static final String GROUPS_KEY = "groups";
    private static final String PERMISSIONS_KEY = "permissions";

    private DefaultUser(Builder builder) {
        super(builder.username, builder.getProperties());
    }

    //for jongo
    protected DefaultUser() {
    }

    public static Builder username(String username) {
        return new Builder(username);
    }

    @JsonIgnore
    public String getFirstName() {
        return (String) get(FIRST_KEY);
    }

    @JsonIgnore
    public String getLastName() {
        return (String) get(LAST_KEY);
    }

    @JsonIgnore
    public String getEmail() {
        return (String) get(EMAIL_KEY);
    }

    @JsonIgnore
    public boolean isInGroup(String group) {
        return ((List) get(GROUPS_KEY)).contains(group);
    }

    @JsonIgnore
    public List<String> getGroups() {
        return Collections.unmodifiableList((List<String>) get(GROUPS_KEY));
    }

    @JsonIgnore
    public boolean hasPermission(String permission) {
        return ((List) get(PERMISSIONS_KEY)).contains(permission);
    }

    @JsonIgnore
    public List<String> getPermissions() {
        return Collections.unmodifiableList((List<String>) get(PERMISSIONS_KEY));
    }

    /**
     * A builder to create a DefaultUser - this class should not be used directly but obtained via
     * {@link DefaultUser#username(java.lang.String)} instead.
     */
    public static class Builder {

        private static final Set<String> INVALID_KEYS = new HashSet<>();

        static {
            Collections.addAll(INVALID_KEYS, PERMISSIONS_KEY, GROUPS_KEY, FIRST_KEY, LAST_KEY);
        }
        private final String username;
        private final Set<String> permissions = new LinkedHashSet<>();
        private final Set<String> groups = new LinkedHashSet<>();
        private final Map<String, Object> properties = new LinkedHashMap<>();

        private Builder(String username) {
            this.username = Objects.requireNonNull(username, "username can't be null");
            properties.put(FIRST_KEY, "");
            properties.put(LAST_KEY, "");
            properties.put(EMAIL_KEY, "");
            properties.put(GROUPS_KEY, new ArrayList<String>());
            properties.put(PERMISSIONS_KEY, new ArrayList<String>());
        }

        /**
         *
         * @param first the first name of this user - uses "first" as a key in mongo
         */
        public Builder first(String first) {
            return put(FIRST_KEY, first);
        }

        /**
         *
         * @param last the last name of this user - uses "last" as a key in mongo
         */
        public Builder last(String last) {
            return put(LAST_KEY, last);
        }

        /**
         *
         * @param email the email of this user - uses "email" as a key in mongo
         */
        public Builder email(String email) {
            return put(EMAIL_KEY, email);
        }

        /**
         *
         * @param groups the list of groups to which the user belongs - uses "groups" as a key in mongo
         */
        public Builder inGroups(String... groups) {
            Objects.requireNonNull(groups, "groups can't be null");
            if (Arrays.asList(groups).contains(null)) {
                throw new IllegalArgumentException("null is not a valid group");
            }
            Collections.addAll(this.groups, groups);
            return this;
        }

        /**
         *
         * @param permissions the list of permissions assigned to the user - uses "permissions" as a key in mongo
         */
        public Builder withPermissions(String... permissions) {
            Objects.requireNonNull(permissions, "groups can't be null");
            if (Arrays.asList(permissions).contains(null)) {
                throw new IllegalArgumentException("null is not a valid group");
            }
            Collections.addAll(this.permissions, permissions);
            return this;
        }

        /**
         *
         * @param property the name of the property
         * @param value    the value to store for this user
         *
         * @throws IllegalArgumentException If the property name is already in use. The following names are not allowed:
         *                                  "username", "permissions", "groups", "first", "last", "hash", "salt", "_id"
         */
        public Builder with(String property, Object value) {
            Objects.requireNonNull(property, "property can't be null");
            checkKey(property, INVALID_KEYS);
            properties.put(property, value);
            return this;
        }

        /**
         * Creates a new user using the details provided to the builder.
         */
        public DefaultUser create() {
            return new DefaultUser(this);
        }

        private Map<String, Object> getProperties() {
            Map<String, Object> allProperties = new LinkedHashMap<>();
            allProperties.putAll(properties);
            ((List<String>) allProperties.get(GROUPS_KEY)).addAll(groups);
            ((List<String>) allProperties.get(PERMISSIONS_KEY)).addAll(permissions);
            return allProperties;
        }

        private Builder put(String key, Object value) {
            Objects.requireNonNull(value, key + "can't be null");
            properties.put(key, value);
            return this;
        }
    }
}
