/*
 * Copyright 2013 Yann Le Tallec.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.assylias.mongo.connector;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

/**
 * A User with a user name and a set of properties. This class can be extended to refine the type of properties
 * available. See for example {@link DefaultUser}.
 *
 */
public class User {

    private static final Set<String> INVALID_KEYS = new HashSet<>();
    static {
        Collections.addAll(INVALID_KEYS, "user", "hash", "salt", "_id");
    }

    @JsonProperty("user")
    private final String user;
    private final Map<String, Object> properties = new LinkedHashMap<>();

    //for Jongo
    protected User() {
        this.user = null;
    }

    /**
     * @param user       the user name
     * @param properties the properties of this user, such as first name, email address or permissions
     *
     * @throws NullPointerException if user or properties is null
     */
    public User(String user, Map<String, Object> properties) {
        this.user = Objects.requireNonNull(user, "user can't be null");
        Objects.requireNonNull(properties, "properties can't be null");
        this.properties.putAll(properties);
    }

    /**
     * @param user       the user name
     * @param properties the properties of this user, such as first name, email address or permissions. The properties
     *                   go by two, the first string is the property name and the second its value
     *
     * @throws NullPointerException if user or properties is null
     */
    public User(String user, String... properties) {
        this.user = Objects.requireNonNull(user, "user can't be null");
        Objects.requireNonNull(properties, "properties can't be null");
        if (properties.length % 2 != 0) {
            throw new IllegalArgumentException("properties must contain an even number of parameters");
        }
        for (int i = 0; i < properties.length - 1; i += 2) {
            checkKey(properties[i], INVALID_KEYS);
            this.properties.put(properties[i], properties[i + 1]);
        }
    }

    /**
     * Returns the value of the given property for this user
     *
     * @param property the property identifier
     *
     * @return the value of the given property for this user
     */
    public Object get(String property) {
        return properties.get(property);
    }

    /**
     * Verifies that the provided key is valid - for example "_id" is always invalid.
     * @param key the key to verify
     * @param invalidKeys the list of invalid keys, on top of those invalid for any users
     *
     * @throws IllegalArgumentException if the key is invalid.
     */
    static void checkKey(String key, Set<String> invalidKeys) {
        if (invalidKeys.contains(key) || INVALID_KEYS.contains(key)) {
            throw new IllegalArgumentException(key + " is not a valid property name");
        }
    }

    /**
     * Returns the user name of this user.
     *
     * @return the user name
     */
    public String getUser() {
        return user;
    }

    @Override
    public int hashCode() {
        return Objects.hash(user, properties);
    }

    /**
     * Two users are deemed equal if they have the same user name and properties.
     */
    @Override
    public boolean equals(Object obj) {
        //does not test the class equality to allow equality to work when serialising/deserialising a user.
        if (obj == null) return false;
        if (!(obj instanceof User)) return false;
        final User other = (User) obj;
        if (!Objects.equals(this.user, other.user)) return false;
        return Objects.equals(this.properties, other.properties);
    }

    @Override
    public String toString() {
        return user + " " + properties;
    }
}
