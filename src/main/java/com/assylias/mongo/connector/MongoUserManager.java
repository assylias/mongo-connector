/*
 * Copyright 2013 Yann Le Tallec.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.assylias.mongo.connector;

import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBObject;
import com.mongodb.MongoException;
import com.mongodb.util.JSON;
import java.util.List;
import org.bson.types.ObjectId;
import org.jongo.MongoCollection;

/**
 * A class that handles user management (creation, edition, authentication).
 */
public class MongoUserManager implements UserManager {

    private static final String PASSWORD_PROJECTION = "{_id: 0, hash: 1, salt: 1}";

    private final MongoManager<User> mongo;

    public MongoUserManager(DB mongoDb) {
        this.mongo = new MongoManager<>(mongoDb, "users", User.class);
    }

    /**
     * Creates a new user with am associated password
     *
     * @param user     the User to be created
     * @param password the password for that user
     * @return the ObjectId of the new entry in mongo
     *
     * @throws MongoException.DuplicateKey if a user with the same username already exists
     * @throws MongoException              if the user was not saved to the database
     */
    @Override
    public ObjectId create(User user, char[] password) {
        if (mongo.get("{user: \"" + user.getUser() + "\"}") != null) {
            throw new MongoException.DuplicateKey(0, "user " + user.getUser() + " already in use");
        }
        byte[] salt = Passwords.getNextSalt();
        byte[] hash = Passwords.hash(password, salt);
        return mongo.create(user, new BasicDBObject("salt", salt), new BasicDBObject("hash", hash));
    }

    /**
     * Retrieves a user from mongo using its username
     * @param user the user's username
     *
     * @return the User if it exists or null otherwise
     */
    @Override
    public User get(String user) {
        return mongo.get("{user: \"" + user + "\"}");
    }

    /**
     * Retrieves all the users from mongo.
     * @return the list of users
     */
    @Override
    public List<User> getAll() {
        return mongo.getAll();
    }

    /**
     * Amends a user in mongo.
     * @param id the ObjectId of the user in mongo
     * @param user the new user details - the username cannot be changed
     *
     */
    @Override
    public void amend(ObjectId id, User user) {
        mongo.amend(id, user);
    }

    /**
     * Changes the password of the given user in mongo.
     * @param user     the user
     * @param password the new password
     *
     */
    @Override
    public void changePassword(User user, char[] password) {
        byte[] salt = Passwords.getNextSalt();
        byte[] hash = Passwords.hash(password, salt);

        DBObject query = new BasicDBObject("user", user.getUser());
        DBObject update = new BasicDBObject();
        update.put("salt", salt);
        update.put("hash", hash);

        mongo.findAndModify(query, update);
    }

    /**
     * Authenticates a user by verifying that the supplied username and password match a user in mongo.
     * @param user     the user that needs to be authenticated
     * @param password the password provided by the user
     *
     * @return true if the user/password combination is valid, false otherwise
     * @throws MongoException if the connection with mongo failed
     */
    @Override
    public boolean authenticate(User user, char[] password) {
        return authenticate(user.getUser(), password);
    }

    /**
     * Authenticates a user by verifying that the supplied username and password match a user in mongo.
     * @param user     the user name of the user that needs to be authenticated
     * @param password the password provided by the user
     *
     * @return true if the user/password combination is valid, false otherwise
     * @throws MongoException if the connection with mongo failed
     */
    @Override
    public boolean authenticate(String user, char[] password) {
        DBObject stored = getDBCollection().findOne(new BasicDBObject("user", user),
                                                    (DBObject) JSON.parse(PASSWORD_PROJECTION));
        if (stored == null) return false;
        byte[] salt = (byte[]) stored.get("salt");
        byte[] hash = (byte[]) stored.get("hash");
        return Passwords.isExpectedPassword(password, salt, hash);
    }

    DBCollection getDBCollection() {
        return mongo.getDBCollection();
    }

    MongoCollection getMongoCollection() {
        return mongo.getMongoCollection();
    }
}
