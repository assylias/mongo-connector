/*
 * Copyright 2013 Yann Le Tallec.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.assylias.mongo.connector;

import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBObject;
import com.mongodb.MongoException;
import com.mongodb.WriteResult;
import java.util.ArrayList;
import java.util.List;
import org.bson.types.ObjectId;
import org.jongo.Find;
import org.jongo.FindOne;
import org.jongo.Jongo;
import org.jongo.MongoCollection;

/**
 * A default mongo database manager that can create and amend objects in mongo.
 * @param <T> the type of object managed by this manager.
 */
public class MongoManager<T> {

    private final MongoCollection collection;
    private final Class<T> clazz;

    public MongoManager(DB mongoDb, String collectionName, Class<T> clazz) {
        Jongo jongo = new Jongo(mongoDb);
        this.collection = jongo.getCollection(collectionName);
        this.clazz = clazz;
    }

    /**
     * Creates a new object in mongo, including additional information not contained in the Object default marshalling
     * if required.
     *
     * @param object    the Object to be stored in the database
     * @param additions additional information to be stored along with the class fields
     *
     * @return the ObjectId of the object inserted in mongo
     *
     * @throws MongoException.DuplicateKey if an object with the same id already exists in the database
     * @throws MongoException              if the object could not be saved to the database
     */
    public ObjectId create(T object, DBObject... additions) {
        DBObject o = MongoUtils.getDbObject(object);
        for (DBObject a : additions) {
            o.putAll(a);
        }
        WriteResult result = collection.getDBCollection().insert(o);
        if (!result.getLastError().ok()) {
            throw new MongoException(result.getError());
        }
        return (ObjectId) o.get("_id");
    }

    /**
     * @param id the object's _id in the database
     *
     * @return the corresponding object if it exists, null otherwise
     */
    public T get(Object id) {
        FindOne result = collection.findOne("{_id: {$oid: \"" + id + "\"}}").projection("{_id: 0}");
        return result.as(clazz);
    }

    /**
     * @param query the query to run
     *
     * @return the first object that matches the query, or null if none was found
     */
    public T get(String query) {
        return get(query, "{_id: 0}");
    }

    /**
     * @param query      the query to run
     * @param projection the projection to use to enable the mapping back to the java object
     *
     * @return the first object that matches the query, or null if none was found
     */
    private T get(String query, String projection) {
        FindOne result = collection.findOne(query).projection(projection);
        return result.as(clazz);
    }

    /**
     *
     * @return all the objects found in the database
     */
    public List<T> getAll() {
        return getAll("{_id: 0}");
    }

    /**
     *
     * @param projection the projection used to map the result back to the Java objects.
     *
     * @return all the objects found in the database
     */
    private List<T> getAll(String projection) {
        Find result = collection.find().projection(projection);
        List<T> list = new ArrayList<>();
        for (T t : result.as(clazz)) {
            list.add(t);
        }

        return list;
    }

    /**
     * Finds the object in the database using its _id and updates the details that have changed if any.
     *
     * @param id the object to be amended
     * @param newObject the new value for the object
     *
     * @throws IllegalArgumentException if the provided id does not exist in mongo
     */
    public void amend(Object id, T newObject) {
        DBObject o = MongoUtils.getDbObject(newObject);
        o.removeField("_id");
        findAndModify(new BasicDBObject("_id", id), o);
    }

    DBCollection getDBCollection() {
        return collection.getDBCollection();
    }

    MongoCollection getMongoCollection() {
        return collection;
    }

    void findAndModify(DBObject query, DBObject update) {
        DBObject stored = collection.getDBCollection().findAndModify(query, new BasicDBObject("$set", update));
        if (stored == null) {
            throw new IllegalArgumentException("The object does not exist: " + query.get("_id"));
        }
    }
}
