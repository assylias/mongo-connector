/*
 * Copyright 2013 Yann Le Tallec.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.assylias.mongo.connector;

import java.util.List;
import org.bson.types.ObjectId;

/**
 *
 */
public interface UserManager {

    /**
     * Amends a user in mongo.
     * @param id the ObjectId of the user in mongo
     * @param user the new user details - the username cannot be changed
     *
     */
    void amend(ObjectId id, User user);

    /**
     * Authenticates a user by verifying that the supplied username and password match a user in mongo.
     * @param user     the user that needs to be authenticated
     * @param password the password provided by the user
     *
     * @return true if the user/password combination is valid, false otherwise
     * @throws MongoException if the connection with mongo failed
     */
    boolean authenticate(User user, char[] password);

    /**
     * Authenticates a user by verifying that the supplied username and password match a user in mongo.
     * @param user     the user name of the user that needs to be authenticated
     * @param password the password provided by the user
     *
     * @return true if the user/password combination is valid, false otherwise
     * @throws MongoException if the connection with mongo failed
     */
    boolean authenticate(String user, char[] password);

    /**
     * Changes the password of the given user in mongo.
     * @param user     the user
     * @param password the new password
     *
     */
    void changePassword(User user, char[] password);

    /**
     * Creates a new user with am associated password
     *
     * @param user     the User to be created
     * @param password the password for that user
     * @return the ObjectId of the new entry in mongo
     *
     * @throws MongoException.DuplicateKey if a user with the same username already exists
     * @throws MongoException              if the user was not saved to the database
     */
    ObjectId create(User user, char[] password);

    /**
     * Retrieves a user from mongo using its username
     * @param user the user's username
     *
     * @return the User if it exists or null otherwise
     */
    User get(String user);

    /**
     * Retrieves all the users from mongo.
     * @return the list of users
     */
    List<User> getAll();

}
