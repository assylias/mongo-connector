/*
 * Copyright 2013 Yann Le Tallec.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.assylias.mongo.connector;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectReader;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.databind.introspect.VisibilityChecker;
import com.mongodb.BasicDBObject;
import com.mongodb.DBEncoder;
import com.mongodb.DBObject;
import com.mongodb.DefaultDBEncoder;
import com.mongodb.LazyWriteableDBObject;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import org.bson.LazyBSONCallback;
import org.bson.io.BasicOutputBuffer;
import org.bson.io.OutputBuffer;
import org.jongo.marshall.jackson.bson4jackson.MongoBsonFactory;

/**
 * A utility class to transform an object (typically a POJO) into a DBObject and vice versa.
 */
public class MongoUtils {

    private final static ObjectMapper MAPPER = new ObjectMapper(MongoBsonFactory.createFactory());

    static {
        MAPPER.setVisibilityChecker(VisibilityChecker.Std.defaultInstance().withFieldVisibility(
                JsonAutoDetect.Visibility.ANY));
    }

    /**
     * Utility class
     */
    private MongoUtils() { }

    /**
     *
     * @param pojo an object to transform into a DBObject
     * @return a DBObject representation of the object.
     */
    public static DBObject getDbObject(Object pojo) {
        try {
            ObjectWriter writer = MAPPER.writer();
            ByteArrayOutputStream baos = new ByteArrayOutputStream();

            writer.writeValue(baos, pojo);
            DBObject dbo = new LazyWriteableDBObject(baos.toByteArray(), new LazyBSONCallback());
            //turn it into a proper DBObject otherwise it can't be edited.
            DBObject result = new BasicDBObject();
            result.putAll(dbo);
            return result;
        } catch (IOException e) {
            throw new RuntimeException("Can't serialise Object: " + pojo, e);
        }
    }

    /**
     *
     * @param dbObject a DBObject representing an pojo
     * @param clazz the type of the pojo
     * @return a new pojo constructed based on the DBObject
     * @throws RuntimeException if an error occurs while creating the pojo is created
     */
    public static <T> T getPojo(DBObject dbObject, Class<T> clazz) {
        ObjectReader reader = MAPPER.reader(clazz);
        DBEncoder dbEncoder = DefaultDBEncoder.FACTORY.create();
        OutputBuffer buffer = new BasicOutputBuffer();
        dbEncoder.writeObject(buffer, dbObject);

        try {
            T pojo = reader.readValue(buffer.toByteArray());
            return pojo;
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
